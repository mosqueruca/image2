FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=ejercicio2/*.jar
COPY ${JAR_FILE} ejercicio2.jar
ENTRYPOINT ["java","-jar","/ejercicio2.jar"]
EXPOSE 8080

# Instala Swagger-UI
RUN npm install swagger-ui-dist
RUN apk add --no-cache python3 python3-dev py3-pip git
RUN apk add curl

# Copia el archivo swagger.json al contenedor
COPY swagger.json /app
COPY swagger.json /app/node_modules/swagger-ui-dist/