package com.opentrends.ejercicio2.web.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.stereotype.Controller;

@Controller
public class BuildController {
	
	@Autowired
    private RestTemplate restTemplate;
	
	@PostMapping("/builds")
	public ResponseEntity<String> createBuild(@Valid @RequestBody Build build) {
	    // Implementa el c�digo para procesar la instancia de Build recibida
		
        String result = restTemplate.postForObject("http://ejemplo/build", build, String.class);

	    return ResponseEntity.ok("Build creada exitosamente");
	}
	
}
