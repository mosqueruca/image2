package com.opentrends.ejercicio2.business.domain;

public class Build {

	private Integer buildId;
	
	@Pattern(regexp="^[A-Za-z]*$",message = "Solo letras")
	private String name;
	
	@Pattern(regexp=".*\\\\.*",message = "Necesita al menos un caracter '\'")
	private String pathRepo;
	
	private String version;

	public Integer getBuildId() {
		return buildId;
	}

	public void setBuildId(Integer buildId) {
		this.buildId = buildId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPathRepo() {
		return pathRepo;
	}

	public void setPathRepo(String pathRepo) {
		this.pathRepo = pathRepo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	

}
